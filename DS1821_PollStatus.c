/*! \file  DS1821_PollStatus.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 17, 2015, 1:25 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "DS1821.h"

/*! DS1821_PollStatus - Wait for the DS1821 to become ready */

/*! DS1821_PollStatus() - Wait for the DS1821 to become ready
 *
 * \callgraph
 * \callergraph
 * \param none
 * \return none
 */
void DS1821_PollStatus(void)
{
    int done;

    done = 0;
    while ( !done )
    {
        DS1821_MasterReset();
        DS1821_Command( DS_READSTATUS );
        if ( DS1821_Read() & DS_DONE )
            done = 1;
    }
}

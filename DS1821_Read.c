/*! \file  DS1821_Read.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 17, 2015, 1:23 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "DS1821.h"

/*! DS1821_Read - Read a byte from the DS1821 */

/*! DS1821_Read() - Read a byte from the DS1821
 *
 * \callgraph
 * 
 * \callergraph
 *
 * \param none
 * \return Byte read from DS1821
 */
unsigned char DS1821_Read(void)
{
    int i,result;

    result = 0;
    for ( i=0; i<8; i++ )
    {
        /* Move to the next bit */
        result = result >> 1;

        /*! Send a read pulse to the DS1821 */
        DS1821_TRIS = 0;
        DS1821_LAT = 0;
        PR1821 = T1US;
        TMR1821 = 0;
        IF1821 = 0;
        while (!IF1821)
            ;

        DS1821_LAT = 1;
        DS1821_TRIS = 1;

        /* Reset the timer - The read time slot is 60us, however, the 1821
         * will respond between 1 and 15 us, so we will watch the timer, sample
         * at 7us, and then allow the full 60us to expire before moving on  */
        PR1821 = T60US;
        TMR1821 = 0;
        IF1821 = 0;

        /* Wait a bit for the DS1821 to respond */
        while (TMR1821 < T7US)
            ;

        /* Now sample the DS1821 */
        if (DS1821_PORT)
            result |= 0x80;

        /* Wait for the time slot to expire */
        while ( !IF1821 )
            ;

    }
    return (unsigned char)result;

}

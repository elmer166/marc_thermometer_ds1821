/*! \file  DS1821.h
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date November 20, 2014, 8:21 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef DS1821_H
#define	DS1821_H

#ifdef	__cplusplus
extern "C"
{
#endif

#include <xc.h>

#define DS1821_LAT  _LATB7
#define DS1821_TRIS _TRISB7
#define DS1821_PORT _RB7

/* The following define the timer to be used */
/*! Timer counter */
#define TMR1821 TMR1
/*! Timer PR register */
#define PR1821  PR1
/*! Timer control register */
#define CON1821 T1CON
/*! Timer interrupt flag */
#define IF1821  _T1IF
  
#define US600   788

 /* Timer settings for various times based on 8MHz Fcy */
/*! 1 microsecond count */
#define T1US   8
/*! 7 microsecond count */
#define T7US   20
/*! 60 microsecond count */
#define T60US  425

/*! DS1821 command to accept status */
#define DS_WRITESTATUS  0x0c
/*! DS1821 command to start temperature conversion */
#define DS_STARTCONV    0xee
/*! DS1821 command to read the temperature */
#define DS_READTEMP     0xaa
/*! DS1821 command to send its status */
#define DS_READSTATUS   0xac
/*! DS1821 command to send counter contents */
#define DS_READCOUNTER  0xa0
/*! DS1821 command to load slope accumulator to counter */
#define DS_LOADCOUNTER  0x41
/*! DS1821 command to accept configuration */
#define DS_CONFIG       0x40
/*! DS1821 command to enter one shot mode */
#define DS_ONESHOT      0x01
/*! DS1821 completion bit */
#define DS_DONE         0x80

/*! Initializes the timer used by the DS1821 */
void DS1821_Initialize( void );
/*! Send the master reset to the DS1821 */
void DS1821_MasterReset( void );
/*! Send a command to the DS1821 */
void DS1821_Command( unsigned char );
/*! Wait for the DS1821 to become ready */
void DS1821_PollStatus( void );
/*! Read a byte from the DS1821 */
unsigned char DS1821_Read( void );
/*! Get the current temperature from the DS1821 */
unsigned char DS1821_ReadTemp( void );
/*! Read a 9 bit word from the 1821*/
//int read1821_9( void );
void Delay_us( unsigned int );

#ifdef	__cplusplus
}
#endif

#endif	/* DS1821_H */


/*! \file  Delay_us.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 17, 2015, 1:39 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */


/*! Delay_us - Delay for a specified number of microseconds */

/*!
 *
 */
void Delay_us(unsigned int len)
{
  int i;
  for ( i=0; i<len; i++ )
    ;
}

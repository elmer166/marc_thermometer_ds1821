/*! \file  DS1821_Command.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 17, 2015, 1:27 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "DS1821.h"

/*! DS1821_Command - Send a command to the DS1821 */

/*!
 *
 */
void DS1821_Command( unsigned char command )
{
    int i;

    for ( i=0; i<8; i++ )
    {
        /* Reset the timer */
        PR1821 = T60US;
        TMR1821 = 0;
        IF1821 = 0;

        /* Pull down the pin */
        DS1821_TRIS = 0;
        DS1821_LAT = 0;

        /* Wait for 7 us to expire */
        /* Master must keep bus low for>1us and <15us */
        while ( TMR1821<T7US )
            ;
        if ( command & 0x01 )
            DS1821_LAT = 1;
        /* Wait for the time slot to expire */
        while ( !IF1821 )
            ;

        /* Reset the bus */
        DS1821_LAT = 1;
        DS1821_TRIS = 1;

        /* Next bit */
        command = command >> 1;
    }
    /* Wait 1 microcsecond */
    PR1821 = T1US;
    TMR1821 = 0;
    IF1821 = 0;
    while ( !IF1821 )
        ;
}

/*! \file  DS1821_Initialize.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 17, 2015, 1:32 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "DS1821.h"

/*! DS1821_Initialize - Initialization for the DS1821 */

/*! DS1821_Initialize() - Set the DS1821 port latch high and set the port
 *  to be an input.  (It will later be set to be an output, but it mist
 *  be already high when that happens.)
 *
 *  Then initialize the timer that will be used.
 *
 * \param none
 * \return none
 */
void DS1821_Initialize(void)
{
   // Set the DS1821 latch to high and make it an input
  DS1821_LAT  = 1;
  DS1821_TRIS = 1;

   /* Set up timer for DS1821                                              */
    TMR1821 = 0;                /* Clear timer                              */
    PR1821  = 0xfffe;           /* Timer counter to a large number          */
    CON1821 = 0x8000;           /* Fosc/4, 1:1 prescale, start timer        */

}

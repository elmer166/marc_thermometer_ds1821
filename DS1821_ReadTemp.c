/*! \file  DS1821_ReadTemp.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 17, 2015, 1:30 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "DS1821.h"

/*! DS1821_ReadTemp - Get the current temperature from the DS1821  */
/*! DS1821_ReadTemp() - Get the current temperature from the DS1821 
 * 
 * \callgraph
 *
 * \param none
 * \return Centigrade temperature
 */
unsigned char DS1821_ReadTemp(void)
{
    unsigned char result;

    /* Set the DS1821 into one-shot mode */
    DS1821_MasterReset();
    DS1821_Command( DS_WRITESTATUS );
    DS1821_Command( DS_CONFIG | DS_ONESHOT );

    /* Instruct the DS1821 to begin a temperature conversion */
    DS1821_MasterReset();
    DS1821_Command( DS_STARTCONV );

    /* Wait for the conversion to complete */
    DS1821_PollStatus();

    /* Send read temperature command */
    DS1821_MasterReset();
    DS1821_Command( DS_READTEMP );

    /* Read the result */
    result = DS1821_Read();
    return result;

}

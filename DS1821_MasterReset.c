/*! \file  DS1821_MasterReset.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 17, 2015, 1:20 PM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include "DS1821.h"

/*! DS1821_MasterReset - */

/*! DS1821_MasterReset()
 * \callergraph
 * \param none
 * \return none
 */
void DS1821_MasterReset(void)
{

  DS1821_LAT = 1;
  DS1821_TRIS = 0;

  DS1821_LAT = 0;        // drive low for 600us
  DS1821_TRIS = 0;       // DS1821 requires at least 480us, stopwatch
  Delay_us(US600);       // says this will be 599.5us

  DS1821_LAT = 1;        // drive high
  DS1821_TRIS = 1;       // Make an input, pullup will bring high
  while ( DS1821_PORT )  // wait while high
    ;                    // Should be 15-60us (Measured 30)
  while ( !DS1821_PORT ) // wait while low
    ;                    // should be 60-240 us (measured 117)
  return;

}
